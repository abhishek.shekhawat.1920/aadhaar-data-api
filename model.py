from pyzbar.pyzbar import decode
from pyaadhaar.utils import isSecureQr
from pyaadhaar.decode import AadhaarSecureQr
from pyaadhaar.decode import AadhaarOldQr

# img = cv2.imread('QR22.png')
# img = cv2.imread('QR1.png')
# img = cv2.imread('My_QR3.png')

null_data = None

# Base64_Data = ''

def get_Data(img):
    # img = base64.b64decode(str(Base64_Data))
    # img = Image.open(io.BytesIO(img))
    code = decode(img)
    QRData = None
    # print(code)
    for Scanned_data in code:
        if Scanned_data != null_data:
            QRData = Scanned_data.data
            break

    isSecureQR = (isSecureQr(QRData))

    if isSecureQR:
        secure_qr = AadhaarSecureQr(int(QRData))
        decoded_secure_qr_data = secure_qr.decodeddata()
        # print(decoded_secure_qr_data)
        # verifyMobile = secure_qr.verifyMobileNumber()
        # print(verifyMobile)
        # verifyEmail = secure_qr.verifyEmail('')
        # print(verifyEmail)
    return decoded_secure_qr_data

# get_Data(Base64_Data)
