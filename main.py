from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import base64
from io import BytesIO
from PIL import Image
from model import get_Data


app = FastAPI()


class image_base64(BaseModel):
    img_base64: str


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def basetoimage(base):
    try:
        im_bytes = base64.b64decode(base)  # im_bytes is a binary image
        im_file = BytesIO(im_bytes)  # convert image to file-like object
        img = Image.open(im_file)
        return img, 1
    except:
        return None, 0


@app.get("/")
def root():
    return {"message": "API is running"}


@app.post("/getdata")
async def read_image(file: image_base64):
    base64 = file.img_base64
    image, flag = basetoimage(base64)
    result = {"Result": "1"}
    if flag == 1:
        result.update(get_Data(image))
    else:
        result = {"Result": "Please Provide Correct Input"}
    return result